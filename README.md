# RT
The final project from graphics branch in school 42.
The goal is to create a program, that generates scenes according to Raytracing protocol.

### skills ###
- Ray-tracing method
- implementation of OpenCl framework for optimization of scene rendering
- parsing of JSON files
- work in team

### usage ###
./RT [scenes/scene]
